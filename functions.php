<?php

function getPages(){
	// Connect to DB
	//TODO: Check for error
	$conn = new mysqli('localhost',DB_USERNAME,DB_PASSWORD,DB_NAME);
	//Select pages from table
	$pages = $conn->query("SELECT * FROM pages");
	return $pages;
	
}
function showPages(){
	$count=0;
	$currPath;
	$currTitle;
	//get an array of pages from database
	//for each page
	$pages=getPages();
	while ($currPage=$pages->fetch_object()){
		$currPath=$currPage->page_filepath;
		$currTitle=$currPage->page_title;
		//Add the title
		$id = substr($currPath,6,strlen($currPath)-10);
		echo "<div id=\"$id\" class=\"page\">"."<span class=\"subheading\">$currTitle</span>";
		include($currPath);
		echo "</div>";
		//include the page
	}
		
}
function isAuthentic($email, $password){
	// Connect to DB
	//TODO: Check for error
	$conn = new mysqli('localhost',DB_USERNAME,DB_PASSWORD,DB_NAME);
	$result=$conn->query("SELECT * FROM `users` WHERE `user_email`='$email' AND `user_password`='$password'");
	//check number of retrieved result
	if($result->num_rows == 1){
		$user = $result->fetch_object();
		$_SESSION['user']=$user->user_name;
		$_SESSION['loggedin']=true;
		return true;
	}
	
	
	return false;
}
function redirect($message, $page){
	$_SESSION['message']=$message;
	header("Location:  $page");
}
function logout(){
	unset($_SESSION['user']);
	$_SESSION['loggedin']=false;
	//header('Location:  index.php');
	redirect("You are logged out.","index.php");
	exit();
}