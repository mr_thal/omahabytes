<?php
// Start session
session_start();
if(!(isset($_SESSION['loggedin']))){
	$_SESSION['loggedin']=false;
}


// Load configuration files
require_once('config/db.php');

// Load functions
require_once('functions.php');

?>
<!DOCTYPE html>
<html>
	<head>
	<link rel="stylesheet" type="text/css" href="style.css" />
	<script src="http://code.jquery.com/jquery-latest.js"></script>
	<script type="text/javascript">
	$(document).ready(function($) {
		 
		$(".scroll").click(function(event){		
			event.preventDefault();
			var offset =$(this.hash).offset();
			$('html,body').animate({scrollTop:offset.top}, 500);
		});
		setTimeout("$(\"#message\").fadeOut('slow');",2300);
	});
	$()
	function showLogin(){
		//show login panel
		$("#loginPanel").css("display","inline");
	}
	function hideLogin(){
		//hide login panel
		$("#loginPanel").css("display","none");
	}
	</script>
	</head>
	<body>
		<div id="loginPanel">
		<?php include('login.php')?>
		</div>
		<div id="wrapper">
		<div id="message">
		<?php
			if(isset($_SESSION['message'])){
				echo $_SESSION['message'];
				unset ($_SESSION['message']);
			}
		?>
		</div>
		<div id="login">
			<?php 
			//var_dump($_SESSION);
			
			if(isset($_SESSION['user'])){
				echo $_SESSION['user']."|
				<a href=\"login.php?action=logout\">logout</a>|
				<a href=\"admin.php\">admin</a>";
				
				
			}else{
				echo("<a href=\"javascript:showLogin();\">login</a>");
			}
			?>
		</div>
		<nav>
				<ul>
					
						<li><a href="#about" class="scroll">About</a></li>
						<li><a href="#portfolio" class="scroll">Portfolio</a></li>
						<li><a href="#contact" class="scroll">Contact</a></li>
						<li><a href="#latest" class="scroll">News</a></li>
						
					
				</ul>
			</nav>
		
			<header>
				OMAHA BYTES
			</header>
			
			<div id="content">
			<?php
			//Open pages table
			//loop through each page and display the content
				
				showPages();
			?>
			</div>
			<footer>
			&copy; 2012 OmahaBytes
			</footer>
		</div>
	</body>
</html>